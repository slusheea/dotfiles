{
    description = "Teal";

    inputs = {
        nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

        home-manager = {
            url = "github:nix-community/home-manager";
            inputs.nixpkgs.follows = "nixpkgs";
        };

        musnix.url = "github:musnix/musnix";

        nixvim = {
            url = "github:nix-community/nixvim";
            inputs.nixpkgs.follows = "nixpkgs";
        };
    };

    outputs = { nixpkgs, home-manager, musnix, nixvim, ... }: 
    let
        system = "x86_64-linux";
        lib = nixpkgs.lib;
        pkgs = import nixpkgs {
            inherit system;
            config = { 
                allowUnfree = true; 
            };
        };

        musnixPkgs = import musnix.nix {
            inherit pkgs;
            inherit system;
        };

    in {
        nixosConfigurations = {
            teal = lib.nixosSystem { 
                inherit system;
                modules = [
                    ./configuration.nix
                    home-manager.nixosModules.default
                    musnix.nixosModules.default
	                nixvim.nixosModules.nixvim
                ];
            };
        };
    };
}
