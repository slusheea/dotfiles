{
    fileSystems."/" = {
        device = "/dev/disk/by-uuid/29732000-c3a0-4b95-b8e4-67b599bc6ef4";
        fsType = "ext4";
    };

    fileSystems."/boot" = { 
        device = "/dev/disk/by-uuid/4C82-3B2F";
        fsType = "vfat";
        options = [ "fmask=0022" "dmask=0022" ];
    };

    fileSystems."/home/slushee/Documents/Drive" = { 
        device = "/dev/disk/by-uuid/d77c0097-5ab8-4503-b5d7-361c474dc9b4";
        fsType = "ext4";
    };

    swapDevices = [ ];
}
