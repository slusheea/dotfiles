{
    services = {
        xserver.xkb = {
            layout = "us";
            variant = "colemak";
        };

        kanata = {
            enable = true;
            keyboards.default = {
                config = ''
                    (defsrc
                        caps esc mfwd
                    )

                    (deflayer base
                        esc grv mmid
                    )
                '';
            };
        };
    };

    console.useXkbConfig = true;
}
