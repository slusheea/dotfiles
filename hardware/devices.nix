{
    hardware = {
        i2c.enable = true;
        rtl-sdr.enable = true;
    };

    services.udev.extraRules = ''
        # Picoprobe
        ATTRS{idVendor}=="2e8a", ATTRS{idProduct}=="000c", MODE="664", GROUP="dialout"

        # ST-LINK
        ATTRS{idVendor}=="0483", ATTRS{idProduct}=="374b", MODE="664", GROUP="dialout", TAG+="uaccess"
        ATTRS{idVendor}=="0483", ATTRS{idProduct}=="3752", MODE="664", GROUP="dialout", TAG+="uaccess"
    '';
}
