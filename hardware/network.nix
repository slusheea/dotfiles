{ lib, ... }:

{
    networking.hostName = "teal";
    networking.useDHCP = lib.mkDefault true;

    services.resolved = {
        enable = true;
        fallbackDns = [
            "192.168.1.254"
        ];
        extraConfig = ''
            DNSStubListener=no
        '';
    };

    networking.networkmanager.enable = true;

    services.printing.enable = true;
    services.avahi = {
        enable = true;
        nssmdns4 = true;
        openFirewall = true;
    };

    networking.firewall = {
        # kde connect
        allowedTCPPortRanges = [ { from = 1714; to = 1764; } ];
        allowedUDPPortRanges = [ { from = 1714; to = 1764; } ];
    };
}
