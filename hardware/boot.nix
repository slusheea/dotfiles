{
    boot = { 
        loader = {
            systemd-boot.enable = true;
            efi.canTouchEfiVariables = true;
        };
        initrd = { 
            availableKernelModules = [ "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
            kernelModules = [ ];
        };
        kernelModules = [ "kvm-amd" "i2c-dev" "v4l2loopback" ];
        extraModulePackages = [ ];
    };
}
