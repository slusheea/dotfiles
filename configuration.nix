{ pkgs, inputs, ... }:

{
    imports = [ 
        ./hardware
        ./desktop
        ./services.nix
        ./users.nix
        ./programs/global.nix
    ];

    nix = {
        package = pkgs.nixVersions.stable;
        extraOptions = "experimental-features = nix-command flakes";
        gc = {
            automatic = true;
            options = "--delete-older-than 2d"; 
        };
    };

    musnix.enable = true;

    home-manager = {
        extraSpecialArgs = { inherit inputs; };
        users = {
            "slushee" = {
                home = {
                    username = "slushee";
                    homeDirectory = "/home/slushee";
                    stateVersion = "24.11";
                    sessionVariables.EDITOR = "nvim";
                };
                programs.home-manager.enable = true;

                imports = [
                    ./programs
                    ./desktop/gnome
                ];
            };   
        };

        backupFileExtension = "backup";
    };

    system.stateVersion = "24.11";
}
