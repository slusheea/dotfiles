{ config, pkgs, ... }:

let
    plugins = with pkgs; [
        ChowKick
        ChowCentaur
        CHOWTapeModel
        decent-sampler
        lsp-plugins
        surge-XT
        vital zenity # so vital can open a file prompt
        zam-plugins
    ];
in {
    imports = [ 
        ./alacritty
        ./git
        ./nushell
        ./openrgb
    ];

    nixpkgs.config = { 
        allowUnfree = true;
        permittedInsecurePackages = [ "electron-27.3.11" "olm-3.2.16" ];
    };

    home.packages = with pkgs; [
        # Browsers
        librewolf
        tor-browser

        # Creative
        ## Audio
        bitwig-studio

        ## Image
        blender-hip
        inkscape
        krita

        # Chat
        nheko
        signal-desktop
        slack

        # Games
        steam
        prismlauncher

        # Terminal
        bat
        bottom
        eza
        fd
        nushell
        ripgrep
        zoxide

        # Text
        logseq
        neovide
        nerd-fonts.caskaydia-cove 
        noto-fonts-color-emoji
        onlyoffice-bin

        # Engineering
        kicad
        prusa-slicer
        sdrpp
        # picoscope # ;-;

        # Utilities
        helvum
        mpv
        obs-studio
        wl-clipboard
        kanata

        # Developement
        cargo-cache
        cargo-generate
        usbutils
    ] ++ plugins;

    programs.direnv = {
        enable = true;
        enableNushellIntegration = true;
        nix-direnv.enable = true;
        silent = true;
    };

    home.file.".cargo/cargo-generate.toml".text = ''
        [favorites.pico]
        git = "https://gitlab.com/slusheea/rp2040-template"

        [favorites.cpp]
        git = "https://gitlab.com/Slushee/cargo-generates"
        branch = "cpp"
    '';
}
