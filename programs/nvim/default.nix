{ pkgs, ... } :

{
    imports = [ 
        ./keymap.nix
        ./lsp.nix
        ./plugins.nix
    ];

    programs.nixvim = {
        enable = true;
        extraPackages = [ pkgs.wl-clipboard ];

        # Share clipboard with system
        clipboard.register = "unnamedplus";

        opts = {
            # Gui setup
            guifont = "CaskaydiaCove Nerd Font";
            termguicolors = true;
            scrolloff = 8;
            pumheight = 8;

            # Relative number column
            number = true;
            relativenumber = true;

            # Configure tabs and indentations
            breakindent = true;
            smartindent = true;
            expandtab = true;
            tabstop = 4;
            shiftwidth = 4;

            # Shell for ! and toggleterm
            shell = "nu";

            # Make it colorful
            syntax = "on";
        };

        colorschemes.onedark.enable = true;
    };
}
