{ pkgs, ... }:

{
    programs.nixvim.plugins = { 
        lsp = {
            enable = true;
            servers = {
                ccls.enable = true;
                pylsp.enable = true;
                rust_analyzer = {
                    enable = true;
                    installCargo = false;
                    installRustc = false;
                };
            };
        };

        cmp = {
            enable = true;
            autoEnableSources = true;
            settings = {
                mapping = {
                    "<S-Tab>" = "cmp.mapping.select_prev_item()";
                    "<Tab>" = "cmp.mapping.select_next_item()";
                    "<C-d>" = "cmp.mapping.scroll_docs(-4)";
                    "<C-f>" = "cmp.mapping.scroll_docs(4)";
                    "<C-e>" = "cmp.mapping.close()";
                    "<CR>" = "cmp.mapping.confirm({ select = true })";
                };
                sources = [
                    { name = "buffer"; }
                    { name = "path"; }
                    { name = "nvim_lsp"; }
                ];
                completion.keyword_lenghth = 3;
            };
        };   
    };                     
}                          
                           
                           
                           
                           
