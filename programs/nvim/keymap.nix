{
    programs.nixvim = {
        globals.mapleader = " ";

        keymaps = [
            # Built in shortcuts
            { key = "<leader>bn"; action = "<cmd>bn<CR>zz"; }
            { key = "<leader>bp"; action = "<cmd>bp<CR>zz"; }
            { key = "<leader>tn"; action = "<cmd>tabn<CR>"; }
            { key = "<leader>tp"; action = "<cmd>tabp<CR>"; }
            { key = "<leader>w"; action = "<C-W>w"; }

            # NvimTree
            { key = "<leader>tt"; action = "<cmd>NvimTreeToggle<CR>"; }

            # ToggleTerm
            { mode = "t"; key = "<C-t>"; action = "<C-\\><C-n> <cmd>ToggleTerm<CR>"; }
            { mode = "n"; key = "<C-t>"; action = "<cmd>ToggleTerm<CR>"; }

            # LSP
            { mode = "n"; key = "<leader>r"; action.__raw = ''vim.lsp.buf.rename''; }
            { mode = "n"; key = "<leader>h"; action.__raw = ''vim.lsp.buf.hover''; }
            { mode = "n"; key = "<leader>R"; action.__raw = ''vim.lsp.buf.references''; }
            { mode = "n"; key = "<leader>j"; action.__raw = ''vim.lsp.buf.definition''; }
            { mode = ["n" "v"]; key = "<leader>a"; action.__raw = ''vim.lsp.buf.code_action''; }
            { mode = "n"; key = "<leader>f"; action.__raw = 
                ''  function()
                        vim.lsp.buf.format { async = true }
                    end
                ''; 
            }
        ];
    };
}
