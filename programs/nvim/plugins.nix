{ pkgs, ... }:

{
    programs.nixvim = {
        plugins = {
            # Status lines (top and bottom)
            web-devicons.enable = true;
            lualine.enable = true;
            bufferline = {
                enable = true;
                settings.options.separator-style = "slant";
            };

            # Editing help
            nvim-autopairs.enable = true;
            indent-blankline.enable = true;
            comment = { 
                enable = true;
                settings.opleader.line = " tc";
            };

            # Navigation
            bufdelete.enable = true;
            nvim-tree = {
                enable = true;
                disableNetrw = true;
            };

            # Other QoL plugins
            toggleterm.enable = true;
            noice = { 
                enable = true;
                settings = {
                    lsp.override = {
                        "vim.lsp.util.convert_input_to_markdown_lines" = true;
                        "vim.lsp.util.stylize_markdown" = true;
                        "cmp.entry.get_documentation" = true;
                    };
                    presets = {
                        bottom_search = true;
                        command_palette = true;
                        long_message_to_split = true;
                        inc_rename = false;
                        lsp_doc_border = false;
                    };
                };
            };
        };

        extraPlugins = with pkgs.vimPlugins; [
            numb-nvim
        ];
    };
}
