{
    programs.git = {
        enable = true;
        lfs.enable = true;

        userName = "Slushee";
        userEmail = "slushee@duck.com";

        extraConfig.init.defaultBranch = "main";
    };
}
