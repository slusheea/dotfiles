def create_left_prompt [] {
    let left_prompt = if (is-admin) {
        $"(ansi red_bold)($env.PWD)"
    } else {
        $"(ansi white_bold)($env.PWD | path basename)(if (('buildInputs' in $env) and ($env.buildInputs | $in != null)) { ' (nix)' })"
    }

    $left_prompt
}

def create_right_prompt [] {
    let right_prompt = $"(ansi magenta_bold)(echo «)"

    $right_prompt
}

$env.PROMPT_COMMAND = { || create_left_prompt }
$env.PROMPT_COMMAND_RIGHT = { || create_right_prompt }
$env.PROMPT_INDICATOR = { || " › " }
$env.PROMPT_INDICATOR_VI_INSERT = { || $"(ansi purple_bold) » (ansi reset)" }
$env.PROMPT_INDICATOR_VI_NORMAL = { || $"(ansi purple_bold) › (ansi reset)" }
$env.PROMPT_MULTILINE_INDICATOR = { || '' | fill -c ' ' -w (create_left_prompt | ansi strip | str length) | append $"(ansi purple_bold) » (ansi reset)" | str join }

$env.ENV_CONVERSIONS = {
  "PATH": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
  "Path": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
}

$env.NU_LIB_DIRS = [ ($nu.config-path | path dirname | path join 'scripts') ]
$env.NU_PLUGIN_DIRS = [ ($nu.config-path | path dirname | path join 'plugins') ]

# $env.PATH = ($env.PATH | split row (char esep) | prepend '/home/slushee/.cargo/bin')

$env.EZA_COLORS = "di=35"
