# Basic utils
alias cd = __zoxide_z
alias nvi = neovide --fork

alias ls = eza --group-directories-first --icons --color=always --color-scale all -M
def "lsr" [
  depth: int
  path?: path
] {
    if ($path | is-empty) {
        eza --group-directories-first --icons -a -R -L $depth
    } else {
        eza --group-directories-first --icons -a -R -L ($depth + ($path | path split | length)) $path
    }
}
def "tree" [
  depth: int
  path?: path
] {
    if ($path | is-empty) {
        eza --group-directories-first --icons -a -T -L $depth
    } else {
        eza --group-directories-first --icons -a -T -L $depth $path
    }
}

alias cat = bat --theme=OneHalfDark -f
alias bat = bat --theme=OneHalfDark -f

# Shorthands
alias :q = exit
alias server = ssh 192.168.1.254
alias gen = cargo-generate gen

# Managing nix
alias rebuild = sudo nixos-rebuild switch --flake .#

def "update" [] { 
    cd ~/.nixos
    sudo nix flake update
    rebuild 
    cd - 
}

def "autoclean" [] { 
    sudo nix-env --profile /nix/var/nix/profiles/system --list-generations |
    lines |
    parse "  {gen}  {other}" |
    reject other |
    first (($in | length) - 1) |
    each {|gen| sudo nix-env --profile /nix/var/nix/profiles/system --delete-generations $gen.gen }
    sudo nix-collect-garbage --delete-older-than 2d 
    cargo-cache -e
}

# Power options
alias off = systemctl poweroff
alias suspend = systemctl suspend
alias hibernate = systemctl hibernate
alias reboot = systemctl reboot
