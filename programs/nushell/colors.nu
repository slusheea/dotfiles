let color_theme = {
    separator: white
    leading_trailing_space_bg: { attr: n }
    header: white
    empty: blue
    bool: { || if $in { 'green_bold' } else { 'red_bold' } }
    int: white
    duration: white
    range: white
    float: white
    string: white
    nothing: white
    binary: white
    cellpath: white
    row_index: green_bold
    record: white
    list: white
    block: white
    hints: dark_gray

    shape_and: purple_bold
    shape_binary: purple_bold
    shape_block: blue_bold
    shape_bool: light_cyan_bold
    shape_custom: white
    shape_datetime: cyan_bold
    shape_directory: cyan
    shape_external: white
    shape_externalarg: white
    shape_filepath: cyan
    shape_flag: blue_bold
    shape_float: purple_bold
    shape_garbage: { fg: "#FFFFFF" bg: "#FF0000" attr: b}
    shape_globpattern: white_bold
    shape_int: purple_bold
    shape_internalcall: white_bold
    shape_list: cyan_bold
    shape_literal: blue
    shape_matching_brackets: { attr: u }
    shape_nothing: light_cyan
    shape_operator: yellow
    shape_or: purple_bold
    shape_pipe: purple_bold
    shape_range: yellow_bold
    shape_record: cyan_bold
    shape_redirection: purple_bold
    shape_signature: green_bold
    shape_string: white
    shape_string_interpolation: cyan_bold
    shape_table: blue_bold
    shape_variable: purple
}
