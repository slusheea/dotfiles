## cargo completions start
def "nu-complete cargo targets" [type: string] {
  ^cargo metadata --format-version=1 --offline --no-deps | from json | get packages.targets | flatten | where ($type in $it.kind) | get name
}
def "nu-complete cargo bins" [] { nu-complete cargo targets bin }
def "nu-complete cargo examples" [] { nu-complete cargo targets example }

def "nu-complete cargo packages" [] {
  let metadata = (^cargo metadata --format-version=1 --offline --no-deps)
  if $metadata == '' {
    []
  } else {
    $metadata | from json | get workspace_members | split column ' ' | get column1
  }
}

def "nu-complete cargo color" [] {
  ['auto', 'always', 'never']
}

def "nu-complete cargo profiles" [] {
  open Cargo.toml | get profile | transpose | get column0
}

def "nu-complete cargo features" [] {
  open Cargo.toml | get features | transpose | get column0
}

def "nu-complete cargo subcommands" [] {
  ^cargo --list | lines | skip 1 | str join "\n" | from ssv --noheaders | get column1
}
def "nu-complete cargo vcs" [] {
  [
    'git',
    'hg',
    'pijul',
    'fossil',
    'none'
  ]
}

# Compile the current package
export extern "cargo build" [
  --package(-p): string@"nu-complete cargo packages"  # Build only the specified packages
  --workspace         # Build all members in the workspace
  --exclude: string@"nu-complete cargo packages" # Exclude the specified packages
  --lib               # Build the package's library
  --bin: string@"nu-complete cargo bins" # Build the specified binary
  --bins              # Build all binary targets
  --example: string@"nu-complete cargo examples" # Build the specified example
  --examples          # Build all example targets
  --test: string      # Build the specified integration test
  --tests             # Build all targets in test mode that have the test = true manifest flag set
  --bench: string     # Build the specified benchmark
  --benches           # Build all targets in benchmark mode that have the bench = true manifest flag set
  --all-targets       # Build all targets
  --features(-F): string@"nu-complete cargo features" # Space or comma separated list of features to activate
  --all-features      # Activate all available features of all selected packages
  --no-default-features # Do not activate the default feature of the selected packages
  --target: string    # Build for the given architecture.
  --release(-r)       # Build optimized artifacts with the release profile
  --profile: string@"nu-complete cargo profiles" # Build with the given profile
  --ignore-rust-version # Ignore `rust-version` specification in packages
  --timings: string    # Output information how long each compilation takes
  --target-dir: path  # Directory for all generated artifacts and intermediate files
  --out-dir: path     # Copy final artifacts to this directory
  --verbose(-v)      # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q)        # Do not print cargo log messages
  --color: string@"nu-complete cargo color"  # Control when colored output is used
  --message-format: string # The output format for diagnostic messages
  --build-plan # Outputs a series of JSON messages to stdout that indicate the commands to run the build
  --manifest-path: path  # Path to the Cargo.toml file
  --frozen           # Require Cargo.lock and cache are up to date
  --locked           # Require Cargo.lock is up to date
  --offline          # Run without accessing the network
  -Z: any            # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
  -h, --help         # Print help information
  --jobs(-j): number # Number of parallel jobs to run
  --future-incompat-report # Displays a future-incompat report for any future-incompatible warnings
]

# Check the current package
export extern "cargo check" [
  --package(-p): string@"nu-complete cargo packages" #Check only the specified packages
  --workspace # Check all members in the workspace
  --all # Alias for --workspace (deprecated)
  --exclude: string@"nu-complete cargo packages" # Exclude the specified packages
  --lib # Check the package's library
  --bin: string@"nu-complete cargo bins" # Check the specified binary
  --example: string@"nu-complete cargo examples" # Check the specified example
  --examples # Check all example targets
  --test: string # Check the specified integration test
  --tests # Check all targets in test mode that have the test = true manifest flag set
  --bench: string # Check the specified benchmark
  --benches # Check all targets in benchmark mode that have the bench = true manifest flag set
  --all-targets # Check all targets
  --features(-F): string@"nu-complete cargo features" # Space or comma separated list of features to activate
  --all-features # Activate all available features
  --no-default-features # Do not activate the `default` feature
  --target: string # Check for the given architecture
  --release(-r) # Check optimized artifacts with the release profile
  --profile: string@"nu-complete cargo profiles" # Check with the given profile
  --ignore-rust-version # Ignore `rust-version` specification in packages
  --timings: string    # Output information how long each compilation takes
  --target-dir: path  # Directory for all generated artifacts and intermediate files
  --verbose(-v)      # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q)        # Do not print cargo log messages
  --color: string@"nu-complete cargo color"  # Control when colored output is used
  --message-format: string # The output format for diagnostic messages
  --manifest-path: path  # Path to the Cargo.toml file
  --frozen           # Require Cargo.lock and cache are up to date
  --locked           # Require Cargo.lock is up to date
  --offline          # Run without accessing the network
  -Z: any            # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
  -h, --help         # Print help information
  --jobs(-j): number # Number of parallel jobs to run
  --keep-going # Build as many crates in the dependency graph as possible
  --future-incompat-report # Displays a future-incompat report for any future-incompatible warnings
]

# Remove the target directory
export extern "cargo clean" [
  --package(-p): string@"nu-complete cargo packages"    # Clean only the specified packages
  --doc                    # Remove only the doc directory in the target directory
  --release                # Remove all artifacts in the release directory
  --profile                # Remove all artifacts in the directory with the given profile name
  --target-dir: path       # Directory for all generated artifacts and intermediate files
  --target: string         # Clean for the given architecture
  --verbose(-v)            # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q)              # Do not print cargo log messages
  --color: string@"nu-complete cargo color" # Control when colored output is used
  --message-format: string # The output format for diagnostic messages
  --manifest-path: path    # Path to the Cargo.toml file
  --frozen                 # Require Cargo.lock and cache are up to date
  --locked                 # Require Cargo.lock is up to date
  --offline                # Run without accessing the network
  -Z: any                  # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
  -h, --help               # Print help information
]

# Build a package's documentation
export extern "cargo doc" [
  --open                    # Open the docs in a browser after building them
  --no-deps                 # Do not build documentation for dependencie
  --document-private-items  # Include non-public items in the documentation
  --package(-p): string@"nu-complete cargo packages" # Document only the specified packages
  --workspace               # Document all members in the workspace
  --exclude: string@"nu-complete cargo packages" # Exclude the specified packages
  --lib: string             # Document the package's library
  --bin: string@"nu-complete cargo bins" # Document the specified binary
  --bins                    # Document all binary targets
  --example: string@"nu-complete cargo examples" # Document the specified example
  --examples                # Document all example targets
  --features(-F): string@"nu-complete cargo features" # Space or comma separated list of features to activate
  --all-features            # Activate all available features of all selected packages
  --no-default-features     # Do not activate the default feature of the selected packages
  --target: string          # Document for the given architecture
  --release(-r)             # Document optimized artifacts with the release profile
  --profile: string@"nu-complete cargo profiles" # Document with the given profile
  --ignore-rust-version     # Ignore `rust-version` specification in packages
  --timings: string         # Output information how long each compilation takes
  --target-dir: path        # Directory for all generated artifacts and intermediate files
  --verbose(-v)             # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q)               # Do not print cargo log messages
  --color: string@"nu-complete cargo color" # Control when colored output is used
  --message-format: string  # The output format for diagnostic messages
  --manifest-path: path     # Path to the Cargo.toml file
  --frozen                  # Require Cargo.lock and cache are up to date
  --locked                  # Require Cargo.lock is up to date
  --offline                 # Run without accessing the network
  -Z: any                   # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
  -h, --help                # Print help information
  --jobs(-j): number        # Number of parallel jobs to run
  --keep-going              # Build as many crates in the dependency graph as possible
]

# Create a new cargo package
export extern "cargo new" [
  path: path          # The directory that will contain the project
  --bin               # Create a package with a binary target (src/main.rs) (default)
  --lib               # Create a package with a library target (src/lib.rs)
  --edition: number   # Specify the Rust edition to use (default: 2021)
  --name: string      # Set the package name. Defaults to the directory name.
  --vcs: string@"nu-complete cargo vcs" # Initialize a new VCS repository for the given version control system
  --registry: string  # Name of the registry to use
  --verbose(-v)       # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q)         # Do not print cargo log messages
  --color: string@"nu-complete cargo color"  # Control when colored output is used
  -Z: any             # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
  -h, --help          # Print help information
]

# Create a new cargo package in an existing directory
export extern "cargo init" [
  path: path # The directory that will contain the project
  --bin # Create a package with a binary target (src/main.rs) (default)
  --lib # Create a package with a library target (src/lib.rs)
  --edition: number # Specify the Rust edition to use (default: 2021)
  --name: string # Set the package name. Defaults to the directory name.
  --vcs: string@"nu-complete cargo vcs" # Initialize a new VCS repository for the given version control system
  --registry: string # Name of the registry to use
  --verbose(-v)      # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q)        # Do not print cargo log messages
  --color: string@"nu-complete cargo color"  # Control when colored output is used
  -Z: any            # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
  -h, --help         # Print help information
]

# Run the current cargo package
export extern "cargo run" [
  ...args: any                              # Arguments to be passed to your program
  --bin: string@"nu-complete cargo bins"    # Name of the bin target to run
  --example: string@"nu-complete cargo examples" # Name of the example target to run
  --quiet(-q)                               # Do not print cargo log messages
  --package(-p): string@"nu-complete cargo packages" # Package with the target to run
  --jobs(-j): number                        # Number of parallel jobs, defaults to # of CPUs
  --release                                 # Build artifacts in release mode, with optimizations
  --profile: string@"nu-complete cargo profiles" # Build artifacts with the specified profile
  --features(-F): string@"nu-complete cargo features" # Space or comma separated list of features to activate
  --all-features                            # Activate all available features
  --no-default-features                     # Do not activate the `default` feature
  --target: string                          # Build for the target triple
  --target-dir: path                        # Directory for all generated artifacts
  --manifest-path: path                     # Path to Cargo.toml
  --message-format: string                  # Error format
  --unit-graph                              # Output build graph in JSON (unstable)
  --ignore-rust-version                     # Ignore `rust-version` specification in packages
  --verbose(-v)                             # Use verbose output (-vv very verbose/build.rs output)
  --color: string@"nu-complete cargo color" # Control when colored output is used
  --frozen                                  # Require Cargo.lock and cache are up to date
  --locked                                  # Require Cargo.lock is up to date
  --offline                                 # Run without accessing the network
  --config: string                          # Override a configuration value (unstable)
  -Z: string                                # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
  --help(-h)                                # Prints help information
]

# Run the tests
export extern "cargo test" [
  test_arg_separator?: string
   ...args: any        # Arguments to be passed to the tests
  --no-run       # Compile, but don't run tests
  --no-fail-fast # Run all tests regardless of failure
  --package(-p): string@"nu-complete cargo packages" # Test only the specified packages
  --workspace # Test all members in the workspace
  --exclude: string@"nu-complete cargo packages" # Exclude the specified packages
  --lib # Test the package's library
  --bin: string@"nu-complete cargo bins" # Test only the specified binary
  --bins # Test all binaries
  --example: string@"nu-complete cargo examples" # Test only the specified example
  --examples # Test all examples
  --test: string # Test the specified integration test
  --tests # Test all targets in test mode that have the test = true manifest flag set
  --bench: string # Test the specified benchmark
  --benches # Test all targets in benchmark mode that have the bench = true manifest flag set
  --all-targets # Test all targets
  --doc # Test ONLY the library's documentation
  --features(-F): string@"nu-complete cargo features" # Space or comma separated list of features to activate
  --all-features # Activate all available features of all selected packages
  --no-default-features # Do not activate the default feature of the selected packages
  --target: string # Test for the given architecture
  --release(-r) # Test optimized artifacts with the release profile
  --profile: string@"nu-complete cargo profiles" # Test with the given profile
  --ignore-rust-version # Ignore `rust-version` specification in packages
  --timings: string # Output information how long each compilation takes
  --target-dir: path # Directory for all generated artifacts and intermediate files
  --verbose(-v) # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q) # Do not print cargo log messages
  --color: string@"nu-complete cargo color" # Control when colored output is used
  --message-format: string # The output format for diagnostic messages
  --manifest-path: path # Path to the Cargo.toml file
  --frozen # Require Cargo.lock and cache are up to date
  --locked  # Require Cargo.lock is up to date
  --offline # Run without accessing the network
  --help(-h) # Prints help information
  -Z: any # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
  --jobs(-j): number # Number of parallel jobs to run
  --keep-going # Build as many crates in the dependency graph as possible
  --future-incompat-report # Displays a future-incompat report for any future-incompatible warnings
]

# Execute benchmarks of a package
export extern "cargo bench" [
  bench_option_seperator?: string
  ...options: any # Options to be passed to the benchmarks
  --no-run # Compile, but don't run benchmarks
  --no-fail-fast # Run all benchmarks regardless of failure
  --package(-p): string@"nu-complete cargo packages" # Benchmark only the specified packages
  --workspace # Benchmark all members in the workspace
  --exclude: string@"nu-complete cargo packages" # Exclude the specified packages
  --lib # Benchmark the package's library
  --bin: string@"nu-complete cargo bins" # Benchmark only the specified binary
  --bins # Benchmark all binary targets
  --example: string@"nu-complete cargo examples" # Benchmark only the specified example
  --examples # Benchmark all example targets
  --test: string # Benchmark the specified integration test
  --tests # Benchmark all targets in test mode that have the test = true
  --bench: string # Benchmark the specified benchmark
  --benches # Benchmark all targets in benchmark mode that have the bench = true manifest flag set
  --all-targets # Benchmark all targets
  --features(-F): string@"nu-complete cargo features" # Space or comma separated list of features to activate
  --all-features # Activate all available features of all selected packages
  --no-default-features # Do not activate the default feature of the selected packages
  --target: string # Benchmark for the given architecture
  --profile: string@"nu-complete cargo profiles" # Build artifacts with the specified profile
  --ignore-rust-version # Ignore `rust-version` specification in packages
  --timings: string # Output information how long each compilation takes
  --target-dir: path # Directory for all generated artifacts and intermediate files
  --verbose(-v) # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q) # Do not print cargo log messages
  --color: string@"nu-complete cargo color"  # Control when colored output is used
  --message-format: string # The output format for diagnostic messages
  --build-plan # Outputs a series of JSON messages to stdout that indicate the commands to run the build
  --manifest-path: path  # Path to the Cargo.toml file
  --frozen # Require Cargo.lock and cache are up to date
  --locked # Require Cargo.lock is up to date
  --offline # Run without accessing the network
  -Z: any # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
  -h, --help # Print help information
  --jobs(-j): number # Number of parallel jobs to run
  --keep-going # Build as many crates in the dependency graph as possible
]

# Update dependencies listed in Cargo.lock
export extern "cargo update" [
  --package(-p): string@"nu-complete cargo packages" # Update only the specified packages
  --aggressive # Dependencies of the specified packages are forced to update as well
  --precise: any # Allows you to specify a specific version number to set the package to
  --workspace(-w) # Attempt to update only packages defined in the workspace
  --dry-run # Displays what would be updated but doesn't write the lockfile
  --verbose(-v) # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q) # Do not print cargo log messages
  --color: string@"nu-complete cargo color" # Control when colored output is used
  --manifest-path: path # Path to the Cargo.toml file
  --frozen # Require Cargo.lock and cache are up to date
  --locked  # Require Cargo.lock is up to date
  --offline # Run without accessing the network
  --help(-h) # Prints help information
  -Z: any # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
]

# Search packages in crates.io
export extern "cargo search" [
  query: string # The thing to search
  --limit: number # Limit the number of results. (default: 10, max: 100)
  --index: string # The URL of the registry index to use
  --registry: string # Name of the registry to use
  --verbose(-v) # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q) # Do not print cargo log messages
  --color: string@"nu-complete cargo color" # Control when colored output is used
  --help(-h) # Prints help information
  -Z: any          # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
]

# Package and upload a package to the registry
export extern "cargo publish" [
  --dry-run # Perform all checks without uploading
  --token: any # API token to use when authenticating
  --no-verify # Don't verify the contents by building them
  --allow-dirty # Allow working directories with uncommitted VCS changes to be packaged
  --index: string # The URL of the registry index to use
  --registry: string # Name of the registry to publish to
  --package(-p): string@"nu-complete cargo packages" # The package to publish
  --target: string # Publish for the given architecture
  --target-dir: path # Directory for all generated artifacts and intermediate files
  --features(-F): string@"nu-complete cargo features" # Space or comma separated list of features to activate
  --all-features # Activate all available features of all selected packages
  --no-default-features # Do not activate the default feature of the selected packages
  --verbose(-v) # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q) # Do not print cargo log messages
  --color: string@"nu-complete cargo color" # Control when colored output is used
  --manifest-path: path # Path to the Cargo.toml file
  --frozen # Require Cargo.lock and cache are up to date
  --locked  # Require Cargo.lock is up to date
  --offline # Run without accessing the network
  --help(-h) # Prints help information
  -Z: any # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
  --jobs(-j): number # Number of parallel jobs to run
  --keep-going # Build as many crates in the dependency graph as possible
]

# Build and install a Rust binary
export extern "cargo install" [
  crate?: string # The crate to install
  --version: string # Specify a version to install
  --vers: string    # Specify a version to install
  --git: string # Git URL to install the specified crate from
  --branch: string # Branch to use when installing from git
  --tag: string # Tag to use when installing from git
  --rev: string # Specific commit to use when installing from git
  --path: path # Filesystem path to local crate to install
  --list # List all installed packages and their versions
  --force(-f) # Force overwriting existing crates or binaries
  --no-track # Don't keep track of this package
  --bin: string@"nu-complete cargo bins" # Install only the specified binary
  --bins # Install all binaries
  --example: string@"nu-complete cargo examples" # Install only the specified example
  --examples # Install all examples
  --root: path # Directory to install packages into
  --registry: string # Name of the registry to use
  --index: string # The URL of the registry index to use
  --features(-F): string@"nu-complete cargo features" # Space or comma separated list of features to activate
  --all-features # Activate all available features of all selected packages
  --no-default-features # Do not activate the default feature of the selected packages
  --target: string # Install for the given architecture
  --target-dir: path # Directory for all generated artifacts and intermediate files
  --debug # Build with the dev profile instead the release profile
  --profile: string@"nu-complete cargo profiles" # Build artifacts with the specified profile
  --timings: string # Output information how long each compilation takes
  --frozen # Require Cargo.lock and cache are up to date
  --locked  # Require Cargo.lock is up to date
  --offline # Run without accessing the network
  --jobs(-j): number # Number of parallel jobs to run
  --verbose(-v) # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q) # Do not print cargo log messages
  --color: string@"nu-complete cargo color" # Control when colored output is used
  --message-format: string # The output format for diagnostic messages
  -Z: any            # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
  -h, --help         # Print help information
]

# Remove a Rust binary
export extern "cargo uninstall" [
  package?: string@"nu-complete cargo packages" # Package to uninstall
  --package(-p): string@"nu-complete cargo packages" # Package to uninstall
  --bin: string@"nu-complete cargo bins" # Only uninstall the binary name
  --root: path # Directory to uninstall packages from
  --verbose(-v) # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q) # Do not print cargo log messages
  --color: string@"nu-complete cargo color" # Control when colored output is used
  -Z: any            # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
  -h, --help         # Print help information
]

#*> Other Commands <*#

# Output the resolved dependencies of a package in machine-readable format
export extern "cargo metadata"  [
  --no-deps # Output information only about the workspace members and don't fetch dependencies
  --format-version: number # Specify the version of the output format to use. Currently 1 is the only possible value
  --filter-platform: string  # This filters the resolve output to only include dependencies for the iven target triple
  --features(-F): string@"nu-complete cargo features" # Space or comma separated list of features to activate
  --all-features # Activate all available features of all selected packages
  --no-default-features # Do not activate the default feature of the selected packages
  --verbose(-v) # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q) # Do not print cargo log messages
  --color: string@"nu-complete cargo color" # Control when colored output is used
  --manifest-path: path # Path to the Cargo.toml file
  --frozen # Require Cargo.lock and cache are up to date
  --locked  # Require Cargo.lock is up to date
  --offline # Run without accessing the network
  --help(-h) # Prints help information
  -Z: any # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
]

# Get the help of the given cargo subcommand
export extern "cargo help" [
  subcommand: string@"nu-complete cargo subcommands"
  --color: string@"nu-complete cargo color" # Control when colored output is used
  --config: string # Override a configuration value
  --frozen         # Require Cargo.lock and cache are up to date
  --locked         # Require Cargo.lock is up to date
  --offline        # Run without accessing the network
  --verbose(-v)    # Use verbose output. May be specified twice for "very verbose" output
  -Z: any          # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
]

# A bunch of lints to catch common mistakes and improve your Rust code
export extern "cargo clippy" [
  --no-deps      # Run Clippy only on the given crate, without linting the dependencies
  --fix          # Automatically apply lint suggestions. This flag implies `--no-deps
  --version(-V)  # Prints version information
  --help(-h)     # Prints help information
  --warn(-W)     # Set lint warnings
  --allow(-A)    # Set lint allowed
  --deny(-D)     # Set lint denied
  --forbid(-F)   # Set lint forbidden
  --package(-p): string@"nu-complete cargo packages" #Check only the specified packages
  --workspace # Check all members in the workspace
  --all # Alias for --workspace (deprecated)
  --exclude: string@"nu-complete cargo packages" # Exclude the specified packages
  --lib # Check the package's library
  --bin: string@"nu-complete cargo bins" # Check the specified binary
  --example: string@"nu-complete cargo examples" # Check the specified example
  --examples # Check all example targets
  --test: string # Check the specified integration test
  --tests # Check all targets in test mode that have the test = true manifest flag set
  --bench: string # Check the specified benchmark
  --benches # Check all targets in benchmark mode that have the bench = true manifest flag set
  --all-targets # Check all targets
  --features(-F): string@"nu-complete cargo features" # Space or comma separated list of features to activate
  --all-features # Activate all available features
  --no-default-features # Do not activate the `default` feature
  --target: string # Check for the given architecture
  --release(-r) # Check optimized artifacts with the release profile
  --profile: string@"nu-complete cargo profiles" # Check with the given profile
  --ignore-rust-version # Ignore `rust-version` specification in packages
  --timings: string    # Output information how long each compilation takes
  --target-dir: path  # Directory for all generated artifacts and intermediate files
  --verbose(-v)      # Use verbose output. May be specified twice for "very verbose" output
  --quiet(-q)        # Do not print cargo log messages
  --color: string@"nu-complete cargo color"  # Control when colored output is used
  --message-format: string # The output format for diagnostic messages
  --manifest-path: path  # Path to the Cargo.toml file
  --frozen           # Require Cargo.lock and cache are up to date
  --locked           # Require Cargo.lock is up to date
  --offline          # Run without accessing the network
  -Z: any            # Unstable (nightly-only) flags to Cargo, see 'cargo -Z help' for details
  -h, --help         # Print help information
  --jobs(-j): number # Number of parallel jobs to run
  --keep-going # Build as many crates in the dependency graph as possible
  --future-incompat-report # Displays a future-incompat report for any future-incompatible warnings
  -Z: any
]

# Parameters from cargo update
export extern "cargo install-update" [
  --all(-a)             # Update all packages
  --allow-no-update(-i) # Allow for fresh-installing packages
  --downdate(-d)        # Downdate packages to match latest unyanked registry version
  --force(-f)           # Update all packages regardless if they need updating
  --git(-g)             # Also update git packages
  --help(-h)            # Prints help information
  --list(-l)            # Don't update packages, only list and check if they need an update (all packages by default)
  --quiet(-q)           # No output printed to stdout
  --version(-V)         # Prints version information
  --cargo-dir(-c)       # The cargo home directory. Default: $CARGO_HOME or $HOME/.cargo
  --filter(-s)          # Specify a filter a package must match to be considered
  --install-cargo(-r)   # Specify an alternative cargo to run for installations
  --temp-dir(-t)        # The temporary directory. Default: $TEMP/cargo-update
]

# Parameters from cargo add
export extern "cargo add" [
  --no-default-features   # Disable the default features
  --default-features      # Re-enable the default features
  --features(-F)          # Space or comma separated list of features to activate
  --optional              # Mark the dependency as optional
  --verbose(-v)           # Use verbose output (-vv very verbose/build.rs output)
  --no-optional           # Mark the dependency as required
  --color: string@"nu-complete cargo color" # Coloring: auto, always, never
  --rename                # Rename the dependency
  --locked                # Require Cargo.lock is up to date
  --package(-p)           # Package to modify
  --offline               # Run without accessing the network
  --quiet(-q)             # Do not print cargo log messages
  --config                # Override a configuration value
  --dry-run               # Don't actually write the manifest
  --help(-h)              # Print help information
  --path                  # Filesystem path to local crate to add
  --git                   # Git repository location
  --branch                # Git branch to download the crate from
  --tag                   # Git tag to download the crate from
  --rev                   # Git reference to download the crate from
  --registry              # Package registry for this dependency
  --dev                   # Add as development dependency
  --build                 # Add as build dependency
  --target                # Add as dependency to the given target platform
  ...args
]
## cargo completions end
############################################################################################
## git completions start
def "nu-complete git available upstream" [] {
  ^git branch -a | lines | each { |line| $line | str replace '\* ' "" | str trim }
}

def "nu-complete git remotes" [] {
  ^git remote | lines | each { |line| $line | str trim }
}

def "nu-complete git log" [] {
  ^git log --pretty=%h | lines | each { |line| $line | str trim }
}

# Yield all existing commits in descending chronological order.
def "nu-complete git commits all" [] {
  ^git rev-list --all --remotes --pretty=oneline | lines | parse "{value} {description}"
}

# Yield commits of current branch only. This is useful for e.g. cut points in
# `git rebase`.
def "nu-complete git commits current branch" [] {
  ^git log --pretty="%h %s" | lines | parse "{value} {description}"
}

# Yield local branches like `main`, `feature/typo_fix`
def "nu-complete git local branches" [] {
  ^git branch | lines | each { |line| $line | str replace '* ' "" | str trim }
}

# Yield remote branches like `origin/main`, `upstream/feature-a`
def "nu-complete git remote branches with prefix" [] {
  ^git branch -r | lines | parse -r '^\*?(\s*|\s*\S* -> )(?P<branch>\S*$)' | get branch | uniq
}

# Yield remote branches *without* prefix which do not have a local counterpart.
# E.g. `upstream/feature-a` as `feature-a` to checkout and track in one command
# with `git checkout` or `git switch`.
def "nu-complete git remote branches nonlocal without prefix" [] {
  # Get regex to strip remotes prefixes. It will look like `(origin|upstream)`
  # for the two remotes `origin` and `upstream`.
  let remotes_regex = (["(", ((nu-complete git remotes | each {|r| [$r, '/'] | str join}) | str join "|"), ")"] | str join)
  let local_branches = (nu-complete git local branches)
  ^git branch -r | lines | parse -r (['^[\* ]+', $remotes_regex, '?(?P<branch>\S+)'] | flatten | str join) | get branch | uniq | where {|branch| $branch != "HEAD"} | where {|branch| $branch not-in $local_branches }
}

def "nu-complete git switch" [] {
  (nu-complete git local branches)
  | parse "{value}"
  | insert description "local branch"
  | append (nu-complete git remote branches nonlocal without prefix
            | parse "{value}"
            | insert description "remote branch")
}

def "nu-complete git checkout" [] {
  (nu-complete git local branches)
  | parse "{value}"
  | insert description "local branch"
  | append (nu-complete git remote branches nonlocal without prefix
            | parse "{value}"
            | insert description "remote branch")
  | append (nu-complete git remote branches with prefix
            | parse "{value}"
            | insert description "remote branch")
  | append (nu-complete git commits all)
  | append (nu-complete git files | where description != "Untracked" | select value)
}

# Arguments to `git rebase --onto <arg1> <arg2>`
def "nu-complete git rebase" [] {
  (nu-complete git local branches)
  | parse "{value}"
  | insert description "local branch"
  | append (nu-complete git remote branches with prefix
            | parse "{value}"
            | insert description "remote branch")
  | append (nu-complete git commits all)
}

def "nu-complete git stash-list" [] {
  git stash list | lines | parse "{value}: {description}"
}

def "nu-complete git tags" [] {
  ^git tag | lines
}

# See `man git-status` under "Short Format"
# This is incomplete, but should cover the most common cases.
const short_status_descriptions = {
  ".D": "Deleted"
  ".M": "Modified"
  "!" : "Ignored"
  "?" : "Untracked"
  "AU": "Staged, not merged"
  "MD": "Some modifications staged, file deleted in work tree"
  "MM": "Some modifications staged, some modifications untracked"
  "R.": "Renamed"
  "UU": "Both modified (in merge conflict)"
}

def "nu-complete git files" [] {
  let relevant_statuses = ["?",".M", "MM", "MD", ".D", "UU"]
  ^git status -uall --porcelain=2
  | lines
  | each { |$it|
    if $it starts-with "1 " {
      $it | parse --regex "1 (?P<short_status>\\S+) (?:\\S+\\s?){6} (?P<value>\\S+)"
    } else if $it starts-with "2 " {
      $it | parse --regex "2 (?P<short_status>\\S+) (?:\\S+\\s?){6} (?P<value>\\S+)"
    } else if $it starts-with "u " {
      $it | parse --regex "u (?P<short_status>\\S+) (?:\\S+\\s?){8} (?P<value>\\S+)"
    } else if $it starts-with "? " {
      $it | parse --regex "(?P<short_status>.{1}) (?P<value>.+)"
    } else {
      { short_status: 'unknown', value: $it }
    }
  }
  | flatten
  | where $it.short_status in $relevant_statuses
  | insert "description" { |e| $short_status_descriptions | get $e.short_status}
}

def "nu-complete git built-in-refs" [] {
  [HEAD FETCH_HEAD ORIG_HEAD]
}

def "nu-complete git refs" [] {
  nu-complete git switchable branches
  | parse "{value}"
  | insert description Branch
  | append (nu-complete git tags | parse "{value}" | insert description Tag)
  | append (nu-complete git built-in-refs)
}

def "nu-complete git files-or-refs" [] {
  nu-complete git switchable branches
  | parse "{value}"
  | insert description Branch
  | append (nu-complete git files | where description == "Modified" | select value)
  | append (nu-complete git tags | parse "{value}" | insert description Tag)
  | append (nu-complete git built-in-refs)
}

def "nu-complete git subcommands" [] {
  ^git help -a | lines | where $it starts-with "   " | parse -r '\s*(?P<value>[^ ]+) \s*(?P<description>\w.*)'
}

def "nu-complete git add" [] {
  nu-complete git files
}

# Check out git branches and files
export extern "git checkout" [
  ...targets: string@"nu-complete git checkout"   # name of the branch or files to checkout
  --conflict: string                              # conflict style (merge or diff3)
  --detach(-d)                                    # detach HEAD at named commit
  --force(-f)                                     # force checkout (throw away local modifications)
  --guess                                         # second guess 'git checkout <no-such-branch>' (default)
  --ignore-other-worktrees                        # do not check if another worktree is holding the given ref
  --ignore-skip-worktree-bits                     # do not limit pathspecs to sparse entries only
  --merge(-m)                                     # perform a 3-way merge with the new branch
  --orphan: string                                # new unparented branch
  --ours(-2)                                      # checkout our version for unmerged files
  --overlay                                       # use overlay mode (default)
  --overwrite-ignore                              # update ignored files (default)
  --patch(-p)                                     # select hunks interactively
  --pathspec-from-file: string                    # read pathspec from file
  --progress                                      # force progress reporting
  --quiet(-q)                                     # suppress progress reporting
  --recurse-submodules                            # control recursive updating of submodules
  --theirs(-3)                                    # checkout their version for unmerged files
  --track(-t)                                     # set upstream info for new branch
  -b                                              # create and checkout a new branch
  -B: string                                      # create/reset and checkout a branch
  -l                                              # create reflog for new branch
]

# Download objects and refs from another repository
export extern "git fetch" [
  repository?: string@"nu-complete git remotes" # name of the branch to fetch
  --all                                         # Fetch all remotes
  --append(-a)                                  # Append ref names and object names to .git/FETCH_HEAD
  --atomic                                      # Use an atomic transaction to update local refs.
  --depth: int                                  # Limit fetching to n commits from the tip
  --deepen: int                                 # Limit fetching to n commits from the current shallow boundary
  --shallow-since: string                       # Deepen or shorten the history by date
  --shallow-exclude: string                     # Deepen or shorten the history by branch/tag
  --unshallow                                   # Fetch all available history
  --update-shallow                              # Update .git/shallow to accept new refs
  --negotiation-tip: string                     # Specify which commit/glob to report while fetching
  --negotiate-only                              # Do not fetch, only print common ancestors
  --dry-run                                     # Show what would be done
  --write-fetch-head                            # Write fetched refs in FETCH_HEAD (default)
  --no-write-fetch-head                         # Do not write FETCH_HEAD
  --force(-f)                                   # Always update the local branch
  --keep(-k)                                    # Keep dowloaded pack
  --multiple                                    # Allow several arguments to be specified
  --auto-maintenance                            # Run 'git maintenance run --auto' at the end (default)
  --no-auto-maintenance                         # Don't run 'git maintenance' at the end
  --auto-gc                                     # Run 'git maintenance run --auto' at the end (default)
  --no-auto-gc                                  # Don't run 'git maintenance' at the end
  --write-commit-graph                          # Write a commit-graph after fetching
  --no-write-commit-graph                       # Don't write a commit-graph after fetching
  --prefetch                                    # Place all refs into the refs/prefetch/ namespace
  --prune(-p)                                   # Remove obsolete remote-tracking references
  --prune-tags(-P)                              # Remove any local tags that do not exist on the remote
  --no-tags(-n)                                 # Disable automatic tag following
  --refmap: string                              # Use this refspec to map the refs to remote-tracking branches
  --tags(-t)                                    # Fetch all tags
  --recurse-submodules: string                  # Fetch new commits of populated submodules (yes/on-demand/no)
  --jobs(-j): int                               # Number of parallel children
  --no-recurse-submodules                       # Disable recursive fetching of submodules
  --set-upstream                                # Add upstream (tracking) reference
  --submodule-prefix: string                    # Prepend to paths printed in informative messages
  --upload-pack: string                         # Non-default path for remote command
  --quiet(-q)                                   # Silence internally used git commands
  --verbose(-v)                                 # Be verbose
  --progress                                    # Report progress on stderr
  --server-option(-o): string                   # Pass options for the server to handle
  --show-forced-updates                         # Check if a branch is force-updated
  --no-show-forced-updates                      # Don't check if a branch is force-updated
  -4                                            # Use IPv4 addresses, ignore IPv6 addresses
  -6                                            # Use IPv6 addresses, ignore IPv4 addresses
]

# Push changes
export extern "git push" [
  remote?: string@"nu-complete git remotes",         # the name of the remote
  ...refs: string@"nu-complete git local branches"   # the branch / refspec
  --all                                              # push all refs
  --atomic                                           # request atomic transaction on remote side
  --delete(-d)                                       # delete refs
  --dry-run(-n)                                      # dry run
  --exec: string                                     # receive pack program
  --follow-tags                                      # push missing but relevant tags
  --force-with-lease                                 # require old value of ref to be at this value
  --force(-f)                                        # force updates
  --ipv4(-4)                                         # use IPv4 addresses only
  --ipv6(-6)                                         # use IPv6 addresses only
  --mirror                                           # mirror all refs
  --no-verify                                        # bypass pre-push hook
  --porcelain                                        # machine-readable output
  --progress                                         # force progress reporting
  --prune                                            # prune locally removed refs
  --push-option(-o): string                          # option to transmit
  --quiet(-q)                                        # be more quiet
  --receive-pack: string                             # receive pack program
  --recurse-submodules: string                       # control recursive pushing of submodules
  --repo: string                                     # repository
  --set-upstream(-u)                                 # set upstream for git pull/status
  --signed: string                                   # GPG sign the push
  --tags                                             # push tags (can't be used with --all or --mirror)
  --thin                                             # use thin pack
  --verbose(-v)                                      # be more verbose
]

# Pull changes
export extern "git pull" [
  remote?: string@"nu-complete git remotes",         # the name of the remote
  ...refs: string@"nu-complete git local branches"   # the branch / refspec
  --rebase                                           # rebase current branch on top of upstream after fetching
]

# Switch between branches and commits
export extern "git switch" [
  switch?: string@"nu-complete git switch"        # name of branch to switch to
  --create(-c)                                    # create a new branch
  --detach(-d): string@"nu-complete git log"      # switch to a commit in a detatched state
  --force-create(-C): string                      # forces creation of new branch, if it exists then the existing branch will be reset to starting point
  --force(-f)                                     # alias for --discard-changes
  --guess                                         # if there is no local branch which matches then name but there is a remote one then this is checked out
  --ignore-other-worktrees                        # switch even if the ref is held by another worktree
  --merge(-m)                                     # attempts to merge changes when switching branches if there are local changes
  --no-guess                                      # do not attempt to match remote branch names
  --no-progress                                   # do not report progress
  --no-recurse-submodules                         # do not update the contents of sub-modules
  --no-track                                      # do not set "upstream" configuration
  --orphan: string                                # create a new orphaned branch
  --progress                                      # report progress status
  --quiet(-q)                                     # suppress feedback messages
  --recurse-submodules                            # update the contents of sub-modules
  --track(-t)                                     # set "upstream" configuration
]

# Apply the change introduced by an existing commit
export extern "git cherry-pick" [
  commit?: string@"nu-complete git commits all" # The commit ID to be cherry-picked
  --edit(-e)                                    # Edit the commit message prior to committing
  --no-commit(-n)                               # Apply changes without making any commit
  --signoff(-s)                                 # Add Signed-off-by line to the commit message
  --ff                                          # Fast-forward if possible
  --continue                                    # Continue the operation in progress
  --abort                                       # Cancel the operation
  --skip                                        # Skip the current commit and continue with the rest of the sequence
]

# Rebase the current branch
export extern "git rebase" [
  branch?: string@"nu-complete git rebase"    # name of the branch to rebase onto
  upstream?: string@"nu-complete git rebase"  # upstream branch to compare against
  --continue                                  # restart rebasing process after editing/resolving a conflict
  --abort                                     # abort rebase and reset HEAD to original branch
  --quit                                      # abort rebase but do not reset HEAD
  --interactive(-i)                           # rebase interactively with list of commits in editor
  --onto?: string@"nu-complete git rebase"    # starting point at which to create the new commits
  --root                                      # start rebase from root commit
]

# List or change branches
export extern "git branch" [
  branch?: string@"nu-complete git local branches"               # name of branch to operate on
  --abbrev                                                       # use short commit hash prefixes
  --edit-description                                             # open editor to edit branch description
  --merged                                                       # list reachable branches
  --no-merged                                                    # list unreachable branches
  --set-upstream-to: string@"nu-complete git available upstream" # set upstream for branch
  --unset-upstream                                               # remote upstream for branch
  --all                                                          # list both remote and local branches
  --copy                                                         # copy branch together with config and reflog
  --format                                                       # specify format for listing branches
  --move                                                         # rename branch
  --points-at                                                    # list branches that point at an object
  --show-current                                                 # print the name of the current branch
  --verbose                                                      # show commit and upstream for each branch
  --color                                                        # use color in output
  --quiet                                                        # suppress messages except errors
  --delete(-d)                                                   # delete branch
  --list                                                         # list branches
  --contains: string@"nu-complete git commits all"               # show only branches that contain the specified commit
  --no-contains                                                  # show only branches that don't contain specified commit
  --track(-t)                                                    # when creating a branch, set upstream
]

# List or change tracked repositories
export extern "git remote" [
  --verbose(-v)                            # Show URL for remotes
]

# Add a new tracked repository
export extern "git remote add" [
]

# Rename a tracked repository
export extern "git remote rename" [
  remote: string@"nu-complete git remotes"             # remote to rename
  new_name: string                                     # new name for remote
]

# Remove a tracked repository
export extern "git remote remove" [
  remote: string@"nu-complete git remotes"             # remote to remove
]

# Get the URL for a tracked repository
export extern "git remote get-url" [
  remote: string@"nu-complete git remotes"             # remote to get URL for
]

# Set the URL for a tracked repository
export extern "git remote set-url" [
  remote: string@"nu-complete git remotes"             # remote to set URL for
  url: string                                          # new URL for remote
]

# Show changes between commits, working tree etc
export extern "git diff" [
  rev1_or_file?: string@"nu-complete git files-or-refs"
  rev2?: string@"nu-complete git refs"
  --cached                                             # show staged changes
  --name-only                                          # only show names of changed files
  --name-status                                        # show changed files and kind of change
  --no-color                                           # disable color output
]

# Commit changes
export extern "git commit" [
  --all(-a)                                           # automatically stage all modified and deleted files
  --amend                                             # amend the previous commit rather than adding a new one
  --message(-m)                                       # specify the commit message rather than opening an editor
  --no-edit                                           # don't edit the commit message (useful with --amend)
]

# List commits
export extern "git log" [
  # Ideally we'd allow completion of revisions here, but that would make completion of filenames not work.
  -U                                                  # show diffs
  --follow                                            # show history beyond renames (single file only)
  --grep: string                                      # show log entries matching supplied regular expression
]

# Show or change the reflog
export extern "git reflog" [
]

# Stage files
export extern "git add" [
  ...file: string@"nu-complete git add"               # file to add
  --all(-A)                                           # add all files
  --dry-run(-n)                                       # don't actually add the file(s), just show if they exist and/or will be ignored
  --edit(-e)                                          # open the diff vs. the index in an editor and let the user edit it
  --force(-f)                                         # allow adding otherwise ignored files
  --interactive(-i)                                   # add modified contents in the working tree interactively to the index
  --patch(-p)                                         # interactively choose hunks to stage
  --verbose(-v)                                       # be verbose
]

# Delete file from the working tree and the index
export extern "git rm" [
  -r                                                   # recursive
  --force(-f)                                          # override the up-to-date check
  --dry-run(-n)                                        # Don't actually remove any file(s)
  --cached                                             # unstage and remove paths only from the index
]

# Show the working tree status
export extern "git status" [
  --verbose(-v)                                       # be verbose
  --short(-s)                                         # show status concisely
  --branch(-b)                                        # show branch information
  --show-stash                                        # show stash information
]

# Stash changes for later
export extern "git stash push" [
  --patch(-p)                                         # interactively choose hunks to stash
]

# Unstash previously stashed changes
export extern "git stash pop" [
  stash?: string@"nu-complete git stash-list"          # stash to pop
  --index(-i)                                          # try to reinstate not only the working tree's changes, but also the index's ones
]

# List stashed changes
export extern "git stash list" [
]

# Show a stashed change
export extern "git stash show" [
  stash: string@"nu-complete git stash-list"
  -U                                                  # show diff
]

# Drop a stashed change
export extern "git stash drop" [
  stash?: string@"nu-complete git stash-list"
]

# Create a new git repository
export extern "git init" [
  --initial-branch(-b)                                # initial branch name
]

# List or manipulate tags
export extern "git tag" [
  --delete(-d): string@"nu-complete git tags"         # delete a tag
]

# Prune all unreachable objects
export extern "git prune" [
  --dry-run(-n)                                       # dry run
  --expire: string                                    # expire objects older than
  --progress                                          # show progress
  --verbose(-v)                                       # report all removed objects
]

# Start a binary search to find the commit that introduced a bug
export extern "git bisect start" [
  bad?: string                 # a commit that has the bug
  good?: string                # a commit that doesn't have the bug
]

# Mark the current (or specified) revision as bad
export extern "git bisect bad" [
]

# Mark the current (or specified) revision as good
export extern "git bisect good" [
]

# Skip the current (or specified) revision
export extern "git bisect skip" [
]

# End bisection
export extern "git bisect reset" [
]

# Show help for a git subcommand
export extern "git help" [
  command: string@"nu-complete git subcommands"       # subcommand to show help for
]
## git completions end
############################################################################################
## just completions
def "nu-complete just recipes" [] {
    ^just --unsorted --dump --dump-format json
        | from json
        | get recipes
        | transpose k v
        | each {|x|
            {
                value: $x.k,
                description: ( $x.v.parameters
                             | each {|y|
                                    if ($y.default|is-empty) {
                                        $y.name
                                    } else {
                                        $'($y.name)="($y.default)"'
                                    }
                                }
                             | str join ' '
                             )
            }
        }
}

def "nu-complete just args" [context: string, offset: int] {
    let r = ($context | split row ' ')
    ^just -u --dump --dump-format json
        | from json
        | get recipes
        | get ($r.1)
        | get body
        | each {|x| {description: ($x | get 0) }}
        | prepend ''

}

export def just [
    recipes?: string@"nu-complete just recipes"
    ...args: any@"nu-complete just args"
] {
    if ($recipes | is-empty) {
        ^just
    } else {
        ^just $recipes ...$args
    }
}
## just completions end
############################################################################################
## rustup completions
def "nu-complete rustup" [] {
  ^rustup --help 
  | str replace --regex --multiline '(rustup[\s\S]*(?=SUBCOMMANDS:))' '' 
  | str replace --regex --multiline '\n+DISCUSSION[\s\S]*' ''
  | lines 
  | where $it starts-with "   " 
  | parse -r '\s*(?P<value>[^ ]+) \s*(?P<description>\w.*)'
}

def "nu-complete rustup toolchain" [] {
  ^rustup toolchain 
  | str replace --regex --multiline '(rustup[\s\S]*(?=SUBCOMMANDS:))' '' 
  | str replace --regex --multiline '\n+DISCUSSION[\s\S]*' ''
  | lines 
  | where $it starts-with "   " 
  | parse -r '\s*(?P<value>[^ ]+) \s*(?P<description>\w.*)'
}


def "nu-complete rustup toolchain list" [] {
  ^rustup toolchain list
  | lines
  | str replace " (default)" ""
  | append 'stable'
  | append 'beta'
  | append 'nightly'
}

def "nu-complete rustup target" [] {
  ^rustup target 
  | str replace --regex --multiline '(rustup[\s\S]*(?=SUBCOMMANDS:))' '' 
  | str replace --regex --multiline '\n+DISCUSSION[\s\S]*' ''
  | lines 
  | where $it starts-with "   " 
  | parse -r '\s*(?P<value>[^ ]+) \s*(?P<description>\w.*)'
}

def "nu-complete rustup target list" [] {
  ^rustup target list
  | lines
  | str replace " (installed)" ""
}

def "nu-complete rustup target list --installed" [] {
  ^rustup target list --installed
  | lines
}

def "nu-complete rustup update" [] {
  ^rustup toolchain list 
  | lines 
  | str replace " (default)" ""
}

def "nu-complete rustup component" [] {
  ^rustup component 
  | str replace --regex --multiline '(rustup[\s\S]*(?=SUBCOMMANDS:))' '' 
  | str replace --regex --multiline '\n+DISCUSSION[\s\S]*' ''
  | lines 
  | where $it starts-with "   " 
  | parse -r '\s*(?P<value>[^ ]+) \s*(?P<description>\w.*)'
}

def "nu-complete rustup component list" [] {
  ^rustup component list
  | lines
  | str replace " (installed)" ""
}

def "nu-complete rustup component list installed" [] {
  ^rustup component list 
  | lines 
  | find --regex '^.+(?=\s\(installed)'
}

def "nu-complete rustup override" [] {
  ^rustup override 
  | str replace --regex --multiline '(rustup[\s\S]*(?=SUBCOMMANDS:))' '' 
  | str replace --regex --multiline '\n+DISCUSSION[\s\S]*' ''
  | lines 
  | where $it starts-with "   " 
  | parse -r '\s*(?P<value>[^ ]+) \s*(?P<description>\w.*)'
}

def "nu-complete rustup override list" [] {
  ^rustup override list
  | lines
  # TODO: I don't have any override to match
  # TODO: exclude the result "no overrides"
}

def "nu-complete rustup override list installed" [] {
  ^rustup override list 
  | lines 
  # TODO: I don't have any override to match
  # TODO: exclude the result "no overrides"
}

def "nu-complete rustup self" [] {
  ^rustup self 
  | str replace --regex --multiline '(rustup[\s\S]*(?=SUBCOMMANDS:))' '' 
  | str replace --regex --multiline '\n+DISCUSSION[\s\S]*' ''
  | lines 
  | where $it starts-with "   " 
  | parse -r '\s*(?P<value>[^ ]+) \s*(?P<description>\w.*)'
}

def "nu-complete rustup set" [] {
  ^rustup set 
  | str replace --regex --multiline '(rustup[\s\S]*(?=SUBCOMMANDS:))' '' 
  | str replace --regex --multiline '\n+DISCUSSION[\s\S]*' ''
  | lines 
  | where $it starts-with "   " 
  | parse -r '\s*(?P<value>[^ ]+) \s*(?P<description>\w.*)'
}

def "nu-complete rustup set profile" [] {
  ['minimal', 'default', 'complete']
}

def "nu-complete rustup set auto-self-update" [] {
  ['enable', 'disable', 'check-only']
}

def "nu-complete rustup completions" [] {
  ['bash', 'elvish', 'fish', 'powershell', 'zsh', 'cargo', 'rustup']
}

def "nu-complete rustup completions shell" [] {
  ['bash', 'elvish', 'fish', 'powershell', 'zsh']
}

# ------------------ export extern commands ------------------

export extern "rustup" [
  command?: string@"nu-complete rustup"
    --verbose(-v)    # Enable verbose output
    --quiet(-q)      # Disable progress output
    --help(-h)       # Print help information
    --version(-V)    # Print version information
]

export extern "rustup update" [
  command?: string@"nu-complete rustup update"
  --help(-h)        # Print help information
  --force           # Force an update, even if some components are missing
  --force-non-host  # Install toolchains that require an emulator. See https://github.com/rust-lang/rustup/wiki/Non-host-toolchains
  --no-self-update  # Don't perform self update when running the `rustup update` command
]

export extern "rustup default" [
  command?: string@"nu-complete rustup toolchain list"
]

export extern "rustup toolchain" [
  command?: string@"nu-complete rustup toolchain"
  --help(-h)        # Print help information
]

export extern "rustup toolchain install" [
  command?: string@"nu-complete rustup toolchain list"
  --help(-h)        # Print help information
]


export extern "rustup toolchain uninstall" [
  command?: string@"nu-complete rustup toolchain list"
]

export extern "rustup target" [
  command?: string@"nu-complete rustup target"
  --help(-h)        # Print help information
]

export extern "rustup target add" [
  command?: string@"nu-complete rustup target list"
  --toolchain       # Toolchain name, such as 'stable', 'nightly', or '1.8.0'. For more information see `rustup help toolchain`
  --help(-h)        # Print help information
]

export extern "rustup target remove" [
  command?: string@"nu-complete rustup target list --installed"
]

export extern "rustup component" [
  command?: string@"nu-complete rustup component"
  --help(-h)        # Print help information
]

export extern "rustup component add" [
  command?: string@"nu-complete rustup component list"
  --toolchain       # Toolchain name, such as 'stable', 'nightly', or '1.8.0'. For more information see `rustup help toolchain`
  --target          # <target> TODO: explain this command a bit further
  --help(-h)        # Print help information
]

export extern "rustup component remove" [
  command?: string@"nu-complete rustup component list installed"
  --toolchain       # Toolchain name, such as 'stable', 'nightly', or '1.8.0'. For more information see `rustup help toolchain`
  --target          # <target> TODO: explain this command a bit further
  --help(-h)        # Print help information
]

export extern "rustup override" [
  command?: string@"nu-complete rustup override"
  --help(-h)        # Print help information
]

export extern "rustup override set" [
  command?: string@"nu-complete rustup override list"
  --path            # Path to the directory
  --help(-h)        # Print help information
]

export extern "rustup override unset" [
  command?: string@"nu-complete rustup override list installed"
  --path            # Path to the directory
  --nonexistent     # Remove override toolchain for all nonexistent directories
  --help(-h)        # Print help information
]

export extern "rustup run" [
  command?: string@"nu-complete rustup toolchain list"
]

export extern "rustup self" [
  command?: string@"nu-complete rustup self"
]

export extern "rustup set" [
  command?: string@"nu-complete rustup set"
  --help(-h)        # Print help information
]

export extern "rustup set profile" [
  command?: string@"nu-complete rustup set profile"
  --help(-h)        # Print help information
]

export extern "rustup set auto-self-update" [
  command?: string@"nu-complete rustup set auto-self-update"
  --help(-h)        # Print help information
]

export extern "rustup completions" [
  command?: string@"nu-complete rustup completions"
  --help(-h)        # Print help information
]

export extern "rustup completions rustup" [
  command?: string@"nu-complete rustup completions shell"
  --help(-h)        # Print help information
]

export extern "rustup completions cargo" [
  command?: string@"nu-complete rustup completions shell"
  --help(-h)        # Print help information
]
## rustup completions end
############################################################################################
