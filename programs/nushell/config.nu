source ~/.zoxide.nu
source completions.nu
source aliases.nu
source colors.nu

$env.config = {
  rm: {
    always_trash: false
  }

  history: {
    max_size: 1024
    sync_on_enter: true
    file_format: "plaintext"
  }

  completions: {
    case_sensitive: false
    quick: true
    partial: true
    algorithm: "prefix"
    external: {
      enable: true
      max_results: 25
      completer: null
    }
  }

  table: {
    mode: rounded
    index_mode: auto
    trim: {
      methodology: wrapping
      wrapping_try_keep_words: true
      truncating_suffix: "..."
    }
  }

  explore: {
    help_banner: false
    exit_esc: true

    command_bar_text: '#C4C9C6'

    status_bar_background: {fg: '#1D1F21' bg: '#C4C9C6' }

    highlight: {bg: 'yellow' fg: 'black'}

    table: {
      split_line: '#404040'

      cursor: true

      line_index: true
      line_shift: true
      line_head_top: true
      line_head_bottom: true

      show_head: true
      show_index: true
    }

    config: {
      cursor_color: {bg: 'yellow' fg: 'black'}
    }
  }

  hooks: {
    display_output: { ||
      if (term size).columns >= 100 { table -e } else { table }
    }
  }

  color_config: $color_theme
  footer_mode: 25
  float_precision: 2
  buffer_editor: "nvim"
  use_ansi_coloring: true
  edit_mode: vi
  shell_integration: { reset_application_mode: true }
  show_banner: false
  render_right_prompt_on_last_line: false

  menus: [
      {
        name: completion_menu
        only_buffer_difference: false
        marker: $"(ansi purple_bold) ↓ (ansi reset)"
        type: {
            layout: columnar
            columns: 4
            col_width: 20
            col_padding: 2
        }
        style: {
            text: purple
            selected_text: purple_reverse
            description_text: blue
        }
      }
      {
        name: history_menu
        only_buffer_difference: true
        marker: $"(ansi purple_bold) ↻ (ansi reset)"
        type: {
            layout: list
            page_size: 10
        }
        style: {
            text: purple
            selected_text: purple_reverse
            description_text: blue
        }
      }
  ]
  keybindings: [
    {
      name: completion_menu
      modifier: none
      keycode: tab
      mode: [vi_normal vi_insert]
      event: {
        until: [
          { send: menu name: completion_menu }
          { send: menunext }
        ]
      }
    }
    {
      name: completion_previous
      modifier: shift
      keycode: backtab
      mode: [vi_normal, vi_insert]
      event: { send: menuprevious }
    }
    {
      name: history_menu
      modifier: none
      keycode: char_h
      mode: vi_normal
      event: { send: menu name: history_menu }
    }
    {
      name: search_history
      modifier: control
      keycode: char_f
      mode: [vi_normal, vi_insert]
      event: { send: searchhistory }
    }
  ]
}
