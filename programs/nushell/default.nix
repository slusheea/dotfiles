{
    programs.nushell = {
        enable = true;
        configFile.text = ''
            source ${../nushell}/config.nu
        '';
        envFile.source = ./env.nu;
    };
}
