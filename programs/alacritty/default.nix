{
    programs.alacritty = {
        enable = true;
        
        settings = {
            colors = {
                primary = {
                    foreground = "#FFFFFF";
                    background = "#282A33";
                };

                normal = {
                    black = "#282c34";
                    blue = "#61afef";
                    cyan = "#56b6c2";
                    green = "#98c379";
                    magenta = "#c678dd";
                    red = "#be5046";
                    white = "#FFFFFF";
                    yellow = "#d19a66";
                };

                bright = {
                    black = "#686868";
                    blue = "#61afef";
                    cyan = "#56b6c2";
                    green = "#98c379";
                    magenta = "#c678dd";
                    red = "#e06c75";
                    white = "#FFFFFF";
                    yellow = "#e5c07b";
                };
            };

            font = {
                size = 13.0;

                normal = {
                    family = "CaskaydiaCove Nerd Font";
                    style = "Medium";
                };

                bold = {
                    family = "CaskaydiaCove Nerd Font";
                    style = "Bold";
                };

                italic = {
                    family = "CaskaydiaCove Nerd Font";
                    style = "Italic";
                };
            };

            terminal.shell = "nu";
            env.shell = "nu";
        };
    };
}
