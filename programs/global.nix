{ pkgs, ... }: 

{
    imports = [ 
        ./nvim
    ];

    nixpkgs.config.allowUnfree = true;
    
    programs.steam = {
        enable = true;
        remotePlay.openFirewall = true;
        dedicatedServer.openFirewall = true;
        localNetworkGameTransfers.openFirewall = true;
    };

    programs.gamemode.enable = true;
    
    virtualisation = {
        libvirtd.enable = true;
        docker.enable = true;
        spiceUSBRedirection.enable = true;
    };  
    
    programs.virt-manager.enable = true;
}
