{
    home.file.".config/OpenRGB/plugins/settings/effect-profiles/RGB".text = ''
{
    "Effects": [
        {
            "AllowOnlyFirst": false,
            "AutoStart": true,
            "Brightness": 100,
            "ControllerZones": [
                {
                    "description": "Corsair Lighting Node Device",
                    "is_segment": false,
                    "location": "HID: /dev/hidraw2",
                    "name": "Corsair Lighting Node Core",
                    "reverse": false,
                    "segment_idx": -1,
                    "self_brightness": 100,
                    "serial": "2DC0000CA6047BAFD885605F061C00F5",
                    "vendor": "Corsair",
                    "version": "0.9.16",
                    "zone_idx": 0
                }
            ],
            "CustomName": "FractalMotionFans",
            "CustomSettings": {
                "amplitude": 100.0,
                "background": 10988545,
                "freq_m1": 16.979999542236328,
                "freq_m10": 3.5199999809265137,
                "freq_m11": 0.009999999776482582,
                "freq_m12": 0.019999999552965164,
                "freq_m2": 10.15999984741211,
                "freq_m3": 65.22000122070313,
                "freq_m4": 0.5199999809265137,
                "freq_m5": 6.369999885559082,
                "freq_m6": 8.960000038146973,
                "freq_m7": 0.03999999910593033,
                "freq_m8": 1.0199999809265137,
                "freq_m9": 2.2200000286102295,
                "frequency": 1.2999999523162842,
                "thickness": 8
            },
            "EffectClassName": "FractalMotion",
            "FPS": 60,
            "RandomColors": false,
            "Slider2Val": 1,
            "Speed": 50,
            "Temperature": 0,
            "Tint": 0,
            "UserColors": [
                11010322
            ]
        },
        {
            "AllowOnlyFirst": false,
            "AutoStart": true,
            "Brightness": 100,
            "ControllerZones": [
                {
                    "description": "Logitech Wireless Lightspeed Device",
                    "is_segment": false,
                    "location": "HID: /dev/hidraw1 (Receiver) \r\nWireless Index: 255",
                    "name": "G502 HERO Gaming Mouse",
                    "reverse": false,
                    "segment_idx": -1,
                    "self_brightness": 100,
                    "serial": "",
                    "vendor": "Logitech",
                    "version": "",
                    "zone_idx": 0
                },
                {
                    "description": "Logitech Wireless Lightspeed Device",
                    "is_segment": false,
                    "location": "HID: /dev/hidraw1 (Receiver) \r\nWireless Index: 255",
                    "name": "G502 HERO Gaming Mouse",
                    "reverse": false,
                    "segment_idx": -1,
                    "self_brightness": 100,
                    "serial": "",
                    "vendor": "Logitech",
                    "version": "",
                    "zone_idx": 1
                }
            ],
            "CustomName": "",
            "CustomSettings": {
                "amplitude": 29.170000076293945,
                "background": 11730688,
                "freq_m1": 3.919999837875366,
                "freq_m10": 0.009999999776482582,
                "freq_m11": 0.009999999776482582,
                "freq_m12": 0.07000000029802322,
                "freq_m2": 5.889999866485596,
                "freq_m3": 0.009999999776482582,
                "freq_m4": 0.7899999618530273,
                "freq_m5": 0.009999999776482582,
                "freq_m6": 47.05999755859375,
                "freq_m7": 0.44999998807907104,
                "freq_m8": 1.959999918937683,
                "freq_m9": 56.869998931884766,
                "frequency": 11.460000038146973,
                "thickness": 2
            },
            "EffectClassName": "FractalMotion",
            "FPS": 60,
            "RandomColors": false,
            "Slider2Val": 1,
            "Speed": 50,
            "Temperature": 0,
            "Tint": 0,
            "UserColors": [
                14222412
            ]
        }
    ],
    "version": 2
}
    '';

    home.file.".config/OpenRGB/plugins/settings/EffectSettings.json".text = ''
{
    "audio_settings": {
        "amplitude": 100,
        "audio_device": -1,
        "avg_mode": 0,
        "avg_size": 8,
        "decay": 80,
        "equalizer": [
            1.0,
            1.0,
            1.0,
            1.0,
            1.0,
            1.0,
            1.0,
            1.0,
            1.0,
            1.0,
            1.0,
            1.0,
            1.0,
            1.0,
            1.0,
            1.0
        ],
        "filter_constant": 1.0,
        "nrml_ofst": 0.03999999910593033,
        "nrml_scl": 0.5,
        "window_mode": 0
    },
    "brightness": 100,
    "fps": 60,
    "fpscapture": 60,
    "hide_unsupported": false,
    "prefer_random": false,
    "prefered_colors": [
        0
    ],
    "startup_profile": "RGB",
    "temperature": 0,
    "tint": 0,
    "use_prefered_colors": false
}
    '';
}
