{
    users.users.slushee = {
        isNormalUser = true;
        description = "Slushee";
        extraGroups = [ "networkmanager" "wheel" "audio" "dialout" "libvirtd" "uinput" "plugdev" "docker" ];
    };
}
