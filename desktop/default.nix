{ pkgs, ... }:

{
    imports = [
        ./local.nix
        ./gnome/enable.nix # Remember to change the home manager import if this is changed
    ];

    services.xserver = {
        enable = true;
        excludePackages = [ pkgs.xterm ];
    };

    documentation.nixos.enable = false;
}
