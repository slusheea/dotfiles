{ config, ... }:

{
    dconf.settings = {
        "org/gnome/desktop/applications/terminal" = {
            exec = "alacritty";
            exec-arg = "";
        };

        "org/gnome/desktop/wm/keybindings" = {
            close = ["<Super>w"];
            switch-to-workspace-left = ["<Super>bracketleft"];
            switch-to-workspace-right = ["<Super>bracketright"];
            toggle-fullscreen = ["<Super>f"];
        };

        "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
            name = "Alacritty";
            command = "alacritty";
            binding = "<Super>t";
        };
    };
}
