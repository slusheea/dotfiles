{
    imports = [
        ./extensions.nix
        ./interface.nix
        ./keybindings.nix
    ];
}
