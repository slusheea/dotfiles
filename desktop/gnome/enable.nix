{ pkgs, ... }:

{
    services.xserver = {
        displayManager.gdm = {
            enable = true;
            wayland = true;
        };
        desktopManager.gnome.enable = true;
    };

    services.gnome.core-utilities.enable = false;
    environment.systemPackages = with pkgs; [
        baobab
        eog
        gnome-characters
        gnome-screenshot
        nautilus
    ];
}
