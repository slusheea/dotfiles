{ pkgs, ... }:

let
    extensions = with pkgs.gnomeExtensions; [
        appindicator
        hide-top-bar
        tiling-shell
        gsconnect
    ];

    extensionUuids = map (e: e.extensionUuid) extensions;

in {
    home.packages = extensions;

    dconf.settings."org/gnome/shell" = {
        disable-user-extensions = false;
        enabled-extensions = extensionUuids;
    };
}
