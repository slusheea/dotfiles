{ pkgs, config, ... }:

{   
    gtk = {
        enable = true;
        cursorTheme = {
            package = pkgs.posy-cursors;
            name = "Posy_Cursor_Black";
        };
    };

    dconf = {
        enable = true;
        settings = {
            "org/gnome/desktop/interface" = {
                color-scheme = "prefer-dark";
                enable-hot-corners = "false";
            };
            
            "org/gnome/mutter".edge-tiling = true;
        };
    };
}
