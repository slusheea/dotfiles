{
    time.timeZone = "Europe/Madrid";

    i18n = {
        defaultLocale = "en_US.UTF-8";
        extraLocaleSettings = {
            LC_ADDRESS = "ca_ES.UTF-8";
            LC_IDENTIFICATION = "ca_ES.UTF-8";
            LC_MEASUREMENT = "ca_ES.UTF-8";
            LC_MONETARY = "ca_ES.UTF-8";
            LC_NAME = "ca_ES.UTF-8";
            LC_NUMERIC = "en_US.UTF-8";
            LC_PAPER = "ca_ES.UTF-8";
            LC_TELEPHONE = "es_ES.UTF-8";
            LC_TIME = "ca_ES.UTF-8";
        };
    };
}
