{ pkgs, ...}:

{
    services = {
        hardware.openrgb = {
            enable = true;
            package = pkgs.openrgb-with-all-plugins;
        };
        udev.packages = [ pkgs.openrgb pkgs.gnome-settings-daemon ];

        syncthing = {
            enable = true;
            user = "slushee";
            dataDir = "/home/slushee/";
            configDir = "/home/slushee/.config/syncthing";
            openDefaultPorts = true;
            overrideDevices = true;
            overrideFolders = true;
            settings = {
	            devices = {
                    "lapis" = { id = "KYDS7LA-YKAJQBC-HCT4COA-UKCAAPF-BKFEKQ2-OAYMHOE-V2IVLUE-LTKRMQV"; };
                    "sapphire" = { id = "QZXZWZL-HSRFDZY-ECXX232-ARFZHI5-WDVIZST-WMP7DKI-25I5NNA-5PNUEQR"; };
                };
                folders = {
                    "Music" = {
                        id = "bislh-g0dbu";
                        path = "/home/slushee/Documents/Drive/Music";
                        devices = [ "lapis" "sapphire" ];
                    };
                    "Logseq" = { 
                        id = "jnqj5-wakpu";
                        path = "/home/slushee/Documents/Logseq";
                        devices = [ "lapis" "sapphire" ];
                    };
                };
            };
        };
    };
}
